/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;



import Dao.Dbconect;
import Dao.UsuarioDao;
import Entidad.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author TOSHIBA L835
 */
public class InicioController extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        HttpSession session = request.getSession();
        if(session.getAttribute("usuario")==null){
            request.getRequestDispatcher("/login.jsp").forward(request, response);    
        }
        
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        
         HttpSession session = request.getSession();
        String Nick = request.getParameter("nickname");
        String pass = request.getParameter("passw");
       
        
        Dbconect conect = new Dbconect();
        UsuarioDao usuDao = new UsuarioDao(conect);
        Usuario usuario = usuDao.IncioSesion(Nick, pass);
        conect.disconnect();
        session.setAttribute("usuario", null);
        if(usuario.getIdUsuario()>0){
                Usuario user = new Usuario(Nick,pass);
                session.setAttribute("usuario", user);
                request.getRequestDispatcher("IngresarUsuario.jsp").forward(request, response);
        }else{         
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        } 
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
