/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.Dbconect;
import Dao.RegistroDao;
import Entidad.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author TOSHIBA L835
 */
public class GuardarController extends HttpServlet {

 
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        HttpSession session = request.getSession();
        String nombre,apellido,nickname,contra;
        
        nombre=request.getParameter("nombre");
        apellido=request.getParameter("apel");
        nickname=request.getParameter("nick");
        contra=request.getParameter("pass");
        
        Dbconect conect = new Dbconect();
        RegistroDao regid = new RegistroDao(conect);
        Usuario usu = new Usuario();
        
        if(session.getAttribute("usuario")==null){
            request.getRequestDispatcher("/login.jsp").forward(request, response);    
        }else{
            usu.setNombre(nombre);
            usu.setApellido(apellido);
            usu.setNickname(nickname);
            usu.setContraseña(contra);
            
            
            System.out.println(nombre);
            System.out.println(apellido);
            System.out.println(nickname);
            System.out.println(contra);
            
            regid.Guardar(usu);
            conect.disconnect();
            request.getRequestDispatcher("Registrado.jsp").forward(request, response);
        }
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
