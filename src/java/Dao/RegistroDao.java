package Dao;

import Entidad.Usuario;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author TOSHIBA L835
 */
public class RegistroDao {

    private Dbconect conn;

    public RegistroDao (Dbconect conn) {
        this.conn = conn;
    }
    
    public boolean Guardar(Usuario usuario){
        PreparedStatement guardar;
        
        try{
            guardar = conn.getConnection().prepareStatement(
                    "INSERT INTO usuario (Nickname, Contraseña, Nombre, Apellido)"
                            + "VALUES (?,?,?,?)");
            guardar.setString(1, usuario.getNickname());
            guardar.setString(2, usuario.getContraseña());
            guardar.setString(3, usuario.getNombre());
            guardar.setString(4, usuario.getApellido());
           
            
            guardar.executeUpdate();
            
            return true;
        } catch (SQLException e){
            System.out.println("Error: "+e);
            return false;
       
        }
    }
        
    
}
